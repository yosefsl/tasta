@extends('layouts.app')

@section('content')
<h1 style="padding-left:30px">Add New Customer!</h1>
<form style="padding-left:30px" method="post" action="{{action('CustomerController@store')}}">
@csrf
          <div class="form-group">    
              <label for="title">Name Of Customer</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">    
              <label for="title">Email Of Customer</label>
              <input type="text" class="form-control" name="email"/>
          </div>
          <div class="form-group">    
              <label for="title">Phone Of Customer</label>
              <input type="text" class="form-control" name="phone"/>
          </div>
          <div class="form-group">    
              <input type="submit" class="btn btn-primary" name="submit" value = "Create New Customers"/>
          </div>          
</form> 
@endsection