@extends('layouts.app')

@section('content')


    <H1 style="padding-left:30px">Customers List </H1>
<a style="padding-left:50px" href="{{route('customers.create')}}">Create a new task</a>
<ul>
        @foreach($customers as $customer)
        @if($customer->user->id==$userid)
        <li style="font-weight:bold">
        @else
        <li style="padding-left:15px">
        @endif
            @if($customer->status)
            <span style="color:green">{{$customer->name}} | {{$customer->email}} | {{$customer->phone}} | Created by: {{$customer->user->name}} </span>
            @else
            <span>{{$customer->name}}, {{$customer->email}}, {{$customer->phone}}, Created by: {{$customer->user->name}}</span>
            @endif
            <a style="padding-left:10px" href="{{route('customers.edit', $customer->id)}}">Edit</a> 
            @can('manager')
            <a style="padding:10px" href="{{route('delete', $customer->id)}}">Delete</a> 
            @endcan
            @can('salesrep')
            Delete
            @endcan
            @cannot('salesrep')
            @if ($customer->status == 0)
            @can('manager')
             <a href="{{route('done', $customer->id)}}">Deal Closed</a>
            @endcan 
            @else
            Done!
            @endif
            @endcannot
        </li>
        
        @endforeach
</ul>  
 @endsection