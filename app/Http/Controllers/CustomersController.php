<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\User;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $user = User::Find($id);
        $customers = Customer::All();
        return view('customers.index', ['customers' => $customers],['userid' => $id]);
    }

  
    public function create()
    {
        return view('customers.create'); 
    }

 
    public function store(Request $request)
    {
        $customer = new Customer();
        $id = Auth::id(); 
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->user_id = $id;
        $customer->save();
        return redirect('customers');  
    }


    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {
        $user_id = Auth::id();
        $customer = Customer::find($id);
        if (Gate::denies('manager')) {
            if($customer->user_id != $user_id)
            abort(403,"Sorry, You Don't hold the premission to edit this customer");
        }
        return view('customers.edit', compact('customer'));
    }


    public function update(Request $request, $id)
    {
             if (Gate::denies('manager')) {
             abort(403,"Sorry, You Don't hold the premission to edit this customer..");
          }   
        $customer = Customer::find($id);
        $customer->update($request->except(['_token'])); 
        return redirect('customers');  
    }


    public function done($id)
    {

        if (Gate::denies('manager')) {
            abort(403,"You are not allowed to mark customer as buyer..");
         }          
        $customer = Customer::findOrFail($id);            
        $customer->status = 1; 
        $customer->save();
        return redirect('customers');    
    }    
    
   
    public function destroy($id)
    {
        if (Gate::allows('manager')) {
            abort(403,"You are not allowed to delete customers ..");}
         $customer = Customer::findOrFail($id);
         $customer->delete();
         return redirect('customers'); 
    }


    // public function indexFiltered()
    // {
    //     $id = $id = Auth::id(); //the current user 
    //     $customer = Customer::where('user_id', $id)->get();
    //     $filtered = 1; //this is to mark the view to show link to all tasks 
    //     return view('customers.index', compact('custome  rs','filtered'));
    // }   
}

